#include "../stdafx.h"
#include <windows.h>
#include "GameManager.h"
#include "XmlParser.h"

HINSTANCE ghInstance = NULL;

#define WINDOW_CLASS_NAME L"ui-framework"
#define WINDOW_TITLE L"wooo"

// Edited from sciter's ui-framework sample

bool window::init_class()
{
	static ATOM cls = 0;

	if( cls ) 
		return true;

	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= wnd_proc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= ghInstance;
	wcex.hIcon			= LoadIcon(ghInstance, MAKEINTRESOURCE(IDI_UIFRAMEWORK));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)GetStockObject(GRAY_BRUSH);
	wcex.lpszMenuName	= 0;//MAKEINTRESOURCE(IDC_PLAINWIN);
	wcex.lpszClassName	= WINDOW_CLASS_NAME;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	cls = RegisterClassEx(&wcex);

	return cls != 0;
}

HWND _hwnd;

window::window()
{
  init_class();

  RECT rect;
  SystemParametersInfo(SPI_GETWORKAREA, 0, &rect, 0);

  _hwnd = CreateWindow(
	  WINDOW_CLASS_NAME,
	  WINDOW_TITLE,
	  WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME | WS_MAXIMIZE,
	  0, 0,
	  rect.right, rect.bottom,
	  NULL,
	  NULL,
	  ghInstance,
	  this);

  if (!_hwnd)
     return;

  init();

  ShowWindow(_hwnd, SW_SHOW);
  UpdateWindow(_hwnd);
}

window* window::ptr(HWND hwnd)
{
  return reinterpret_cast<window*>(GetWindowLongPtr(hwnd, GWLP_USERDATA));
}

bool window::init()
{
	SetWindowLongPtr(_hwnd, GWLP_USERDATA, LONG_PTR(this));
	setup_callback(); // callbacks
	sciter::attach_dom_event_handler(_hwnd,this); // dom events and callbacks

	GameManager& g = GameManager::get();
	g.init(ptr(_hwnd));
	SciterSetGlobalAsset(&g);

	load_file(L"res:start_menu.htm");
  
	return true;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//

LRESULT CALLBACK window::wnd_proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if ( message == WM_KEYDOWN && wParam == VK_F5 )
		{
		GameManager::get().StartMenu();
		return 0;
		}

	//SCITER integration starts
	BOOL handled = FALSE;
	LRESULT lr = SciterProcND(hWnd,message,wParam,lParam, &handled);
	if( handled )
		return lr;
	//SCITER integration ends
  
	window* self = ptr(hWnd);
  
	switch (message)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}

UINT DoLoadData(LPSCN_LOAD_DATA pnmld)
{
	LPCBYTE pb = 0; UINT cb = 0;
	aux::wchars wu = aux::chars_of(pnmld->uri);

	printf("%S\n", wu);

	if (wu.like(_TEXT("res:*")))
	{
		// then by calling possibly overloaded load_resource_data method
		if (sciter::load_resource_data(ghInstance, wu.start + 4, pb, cb))
			::SciterDataReady(pnmld->hwnd, pnmld->uri, pb, cb);
	}
	else if (wu.like(_TEXT("this://app/*"))) {
		// try to get them from archive (if any, you need to call sciter::archive::open() first)
		aux::bytes adata = sciter::archive::instance().get(wu.start + 11);
		if (adata.length)
			::SciterDataReady(pnmld->hwnd, pnmld->uri, adata.start, adata.length);
	}
	return LOAD_OK;
}

UINT SC_CALLBACK SciterCallback(LPSCITER_CALLBACK_NOTIFICATION pns, LPVOID callbackParam)
{
	switch (pns->code)
	{
	case SC_LOAD_DATA:
		return DoLoadData((LPSCN_LOAD_DATA)pns);
	}
	return 0;
}

#include "resources.cpp"

int APIENTRY wWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	ghInstance = hInstance;
 
	sciter::debug_output_console console;
	console.printf("");

	::SciterSetCallback(_hwnd, &SciterCallback, NULL);

	MSG msg;
	HACCEL hAccelTable;

	// Perform application initialization:

	sciter::sync::gui_thread_ctx _; // instance of gui_thread_ctx
									// it should be created as a variable inside WinMain 
									// gui_thread_ctx is critical for GUI_CODE_START/END to work
  
	OleInitialize(NULL); // for shell interaction: drag-n-drop, etc.

	sciter::archive::instance().open(aux::elements_of(resources));

	HRSRC hresinfo = FindResource(hInstance, MAKEINTRESOURCE(IDR_XML1), WSTR("XML"));
	if (hresinfo)
	{
		HGLOBAL hRes = LoadResource(hInstance, hresinfo);
		DWORD size = SizeofResource(hInstance, hresinfo);
		LPVOID data = LockResource(hRes);

		if (hRes && size != 0 && data) {
			if (!XmlParser::dialog_doc.Parse((const char*)data)) {
				printf("Error parsing XML. (%s)\n", XmlParser::dialog_doc.ErrorDesc());
			}
		}
		else {
			printf("Error finding dialogs file. (%d)\n", GetLastError());
		}
	}

	window wnd;

	if (!wnd.is_valid())
		return FALSE;

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_UIFRAMEWORK));

	typedef std::function<void(void)> gui_block;

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0)) {
		if (msg.message == WM_NULL)
		{
			gui_block* pf = reinterpret_cast<gui_block*>(msg.lParam);
			sciter::sync::event* pe = reinterpret_cast<sciter::sync::event*>(msg.wParam);
			(*pf)();  // execute the block
			pe->signal(); // signal that we've done with it
							  // this will resume execution of worker thread.
		}
		else if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	GameManager::get().quit();

	OleUninitialize();

	return (int) msg.wParam;
}