#include <sciter/aux-cvt.h>
#include "XmlParser.h"

TiXmlDocument XmlParser::dialog_doc;

std::wstring XmlParser::get_random_option()
{
	const int r = rand() % 2; // 0-2
	int i = 0;
	TiXmlElement* recordTitles = dialog_doc.FirstChildElement("dialogs")->FirstChildElement("test_titles");
	for (TiXmlElement* title = recordTitles->FirstChildElement("title"); title; title = title->NextSiblingElement()) {
		if (i == r) {
			return (std::wstring)aux::a2w(title->GetText());
		}
		else {
			++i;
		}
	}
}

bool XmlParser::load_dialog(const char* id, Dialog& dialog)
{
	TiXmlElement* root = dialog_doc.FirstChildElement("dialogs");

	if (!root) {
		if (dialog_doc.Error())
			std::cout << dialog_doc.ErrorDesc() << std::endl;

		return false;
	}

	for (TiXmlElement* dialogElem = root->FirstChildElement("dialog"); dialogElem; dialogElem = dialogElem->NextSiblingElement())
	{
		if (strcmp(dialogElem->Attribute("id"), id) == 0) {
			dialog.m_id = (std::wstring)aux::a2w(id);
			const char* sclass = dialogElem->Attribute("class");
			if (sclass != 0) {
				dialog.m_style_class = aux::a2w(sclass);
			}

			TiXmlElement* titleElem = dialogElem->FirstChildElement("title");
			if (titleElem) {
				dialog.m_title = aux::a2w(titleElem->GetText());
			}

			dialog.m_message = aux::a2w(dialogElem->FirstChildElement("message")->GetText());
			dialog.m_options.clear();
			TiXmlElement* options = dialogElem->FirstChildElement("options");
			for (TiXmlElement* optionElem = options->FirstChildElement(); optionElem; optionElem = optionElem->NextSiblingElement())
			{
				sciter::value option;
				for (TiXmlAttribute* attr = optionElem->FirstAttribute(); attr; attr = attr->Next())
				{
					const char* name = attr->Name();
					option[(std::wstring)aux::a2w(name)] = optionElem->Attribute(name);
				}
				option[L"text"] = optionElem->GetText();

				dialog.m_options.append(option);
			}
			break;
		}
	}
	return true;
}

std::wstring XmlParser::date_to_str(const date::year_month_day& date)
{
	std::wstringstream ws;
	ws << date;

	return ws.str();
}