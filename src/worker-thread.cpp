#include <dx/handle.h>

#include "window.h"
#include "GameManager.h"

struct thread_params
{
	sciter::value limit; // int
	sciter::value fromFile; // bool
	sciter::value extraParams;// array
	sciter::value onComplete; // callback
};

sciter::value window::init_game(sciter::value fromFile,
								sciter::value onComplete,
								sciter::value extraParams)
{
	thread_params params;
	params.fromFile = fromFile;
	params.extraParams = extraParams;
	params.onComplete = onComplete;

	sciter::thread([](thread_params params) {
		GameManager& g = GameManager::get();

		if (params.fromFile.get(true)) {
			GameManager::load();
		}
		else {
			sciter::value p = params.extraParams;

			if (!(p.is_array() || p.is_object_array())) {
				printf("params != array\n");

				return sciter::value();
			}

			sciter::value ser_members = ((sciter::value)p[5]);
			ser_members.isolate();

			std::map<int, Person> members;
			for (unsigned i = 0, count = ser_members.length(); i < count; ++i) {
				const sciter::value& member = ser_members.key(i),
					data = ser_members[i],
					role = data[0],
					attr_values = data[1];

				std::map<Attribute, float> attrs;
				const int& id = std::stoi(member.get(L""));

				for (unsigned j = 0, len = attr_values.length(); j < len; ++j) {
					attrs[static_cast<Attribute>(j)] = ((sciter::value)attr_values[j]).get(0.0);
				}

				Person character(id, role.get(L""), attrs);
				members[id] = character;
			}

			g.config->load(((sciter::value)p[0]).get(L""),
				((sciter::value)p[1]).get(L""),
				((sciter::value)p[2]).get(L""),
				((sciter::value)p[3]).get(L""),
				((sciter::value)p[4]).get(0),
				members
			);

			//GameManager::save();
		}

	GUI_CODE_START
		params.onComplete.call();

		g.init_game();
	GUI_CODE_END
	}, params);

	return sciter::value();
}

sciter::value window::load_data_table(sciter::value limit, sciter::value onComplete)
{
	thread_params params;
	params.limit = limit;
	params.onComplete = onComplete;

	sciter::thread([](thread_params params) {
		try
		{
			connection c;
			c.open();

			statement s;
			s.prepare(c, "select rowid, name, age, dob, country from data_table order by RANDOM() limit ?;");

			s.bind(1, params.limit.get(0));

			sciter::value result;
			while (s.step())
			{
				sciter::value row;

				row[L"rowid"] = (int)s.get_int64(0);
				row[L"name"] = s.get_wstring(1);
				row[L"age"] = s.get_int(2);
				row[L"dob"] = s.get_wstring(3);
				row[L"country"] = s.get_wstring(4);

				result.append(row);
			}

			GUI_CODE_START
				params.onComplete.call(result);
			GUI_CODE_END
		}
		catch (sql_exception const& e)
		{
			wprintf(L"%d %S\n", e.code, e.message.c_str());
		}
	}, params);

	return sciter::value();
}