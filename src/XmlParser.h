#pragma once

#include <string>
#include <tinyxml/tinyxml.h>
#include "GameManager.h"

class XmlParser
{
public:
	static TiXmlDocument dialog_doc;

	static bool load_dialog(const char* id, Dialog& dialog);
	static std::wstring date_to_str(const date::year_month_day& date);
	static std::wstring get_random_option();
};