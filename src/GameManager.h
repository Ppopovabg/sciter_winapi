#pragma once

#include <map>
#include <functional>

#include <sciter/sciter-x.h>
#include "window.h" // window class
// boost.serialization
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>

#include <tinyxml/tinyxml.h>
#include <dx/handle.h> // SQLite
#include <date/date.h>	// date utils
#include <set>

enum GameState	{ SETUP, PLAYING, INACTIVE };
enum TimerState { RUNNING, PAUSED };
enum Attributes { SKILL1, SKILL2, SKILL3 };

struct Person
{
	int			 m_id;
	std::wstring m_role;
	std::map<Attribute, float> m_attributes;

	template<class Archive> void serialize(Archive& ar, const unsigned int version)
	{
		ar & m_id;
		ar & m_role;
		ar & m_attributes;
	}

	Person() = default;
	Person(int id, std::wstring role, std::map<Attribute, float> attributes)
		: m_id(id), m_role(role), m_attributes(attributes)
	{
	}
};

struct Config
{
	date::year_month_day startedDate;

	sciter::string		playerFirst;
	std::wstring		playerLast;
	std::map<int, Person> m_members;
	std::vector<int>	member_ids;

	Config()  = default;
	~Config() = default;
	
	void load(const std::wstring& p_playerFirst,
			  const std::wstring& p_playerLast,
			  const std::map<int, Person>& p_members)
	{
		playerFirst	= p_playerFirst;
		playerLast		= p_playerLast;
		m_members		= p_members;

		member_ids.reserve(m_members.size());
		for (std::map<int, Person>::iterator it = m_members.begin(); it != m_members.end(); ++it)
			member_ids.push_back(it->first);
	}

	template<class Archive> void serialize(Archive& ar, const unsigned int version)
	{
		ar& playerFirst;
		ar& playerLast;
		ar& m_members;

		if (member_ids.size() == 0) {
			member_ids.reserve(m_members.size());
			for (std::map<int, Person>::iterator it = m_members.begin(); it != m_members.end(); ++it)
				member_ids.push_back(it->first);
		}
	}
};

struct Dialog
{
	sciter::string m_id;
	sciter::string m_title;
	sciter::string m_message;
	sciter::string m_style_class = L"";
	sciter::value  m_options;
};

class GameManager : public sciter::om::asset<GameManager>
{
	window* _wnd;

	GameState _gameState;

	date::year_month_day _gameTime;
	date::year_month_day _eventStart;
	date::year_month_day _eventEnd;
	date::days/*std::chrono::minutes*/ _gameTick;
	date::days	_eventDuration;

	int _ppts = 0;
	int _trophies = 0;
	int _balance = 20000;

	std::vector<std::vector<std::wstring>> _timelineEntries;

	std::map<const std::wstring, std::function<void(const sciter::value&)>> _dialogOpts;

	std::map<date::month_day, unsigned> _activities;

	friend class boost::serialization::access;
	template<class Archive> void serialize(Archive& ar, const unsigned int version)
	{
		ar & config;
		ar & _gameState;
		ar & _gameTime;
		ar & _eventStart;
		ar & _eventEnd;
		ar & _timelineEntries;
	}

	static void CALLBACK timer_proc(PVOID lpParam, BOOLEAN TimerOrWaitFired);
	void gametimer_start();
	void gametimer_tick();
	void gametimer_stop();

	void set_dialog_callbacks();

	void event_start();

public:
	Config* config;

	Dialog current_dialog;

	GameManager() = default;
	virtual ~GameManager() { delete config; }

	static GameManager& get();

	void init(window* wnd);
	void init_game();
	void quit();

	static void save();
	static void load();

	void gui_exec(std::function<void()> gui_block);

	void display_dialog(const char* id);

	template<typename... Args> void display_dialog_arg(const char* id, const char* arg, const Args&... args);
	template<typename... Args> void display_dialog_arg(const char* id, const std::wstring& arg, const Args& ... args);

	void insert_timeline_entry(const std::wstring& title, const date::year_month_day& date);

	// methods accessible from script

	bool SaveFileExists();

	sciter::value PauseTimer();
	sciter::value UnpauseTimer();
	sciter::value StartMenu();

	sciter::value displayDialog(const sciter::string id);
	sciter::value displayDialogArg(const sciter::string id, sciter::value arg);
	sciter::value processDialogResult(const sciter::value opt_data);

	int TimelineEntriesCount() const { return _timelineEntries.size(); };
	std::vector<std::wstring> GetTimelineEntry(const int id) const { return _timelineEntries.at(id); };

	sciter::string	playerFirst()	const { return config->playerFirst; }
	sciter::string	playerLast()	const { return config->playerLast; }
	std::vector<int> members()		const { return config->member_ids; }
	sciter::value	getDataFor(const int id) const;
	sciter::string	getRoleFor(const int id) { return config->m_members[id].m_role; }

	int getCurrentYear();
	int	getCurrentMonth();

	SOM_PASSPORT_BEGIN(GameManager)
		SOM_FUNCS(
			SOM_FUNC(SaveFileExists),
			SOM_FUNC(PauseTimer),
			SOM_FUNC(UnpauseTimer),
			SOM_FUNC(StartMenu),
			SOM_FUNC(displayDialog),
			SOM_FUNC(displayDialogArg),
			SOM_FUNC(processDialogResult),
			SOM_FUNC(playerFirst),
			SOM_FUNC(playerLast),
			SOM_FUNC(members),
			SOM_FUNC(getDataFor),
			SOM_FUNC(getRoleFor),
			SOM_FUNC(TimelineEntriesCount),
			SOM_FUNC(GetTimelineEntry),
			SOM_FUNC(getCurrentYear),
			SOM_FUNC(getCurrentMonth),
		)
	SOM_PASSPORT_END
} ;