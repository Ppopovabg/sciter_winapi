#pragma once

#include "../resource.h"

#include <functional>
#include <sciter/sciter-x.h>
#include <sciter/sciter-x-host-callback.h>
#include <sciter/sciter-x-threads.h>

extern HINSTANCE ghInstance;

class window: public sciter::host<window>,
              public sciter::event_handler
{
  HWND _hwnd;

  static LRESULT CALLBACK	wnd_proc(HWND, UINT, WPARAM, LPARAM);
  static window* ptr(HWND hwnd);
  static bool init_class();
public:
	// notification_handler traits:
  HWND      get_hwnd() { return _hwnd; }
  HINSTANCE get_resource_instance() { return ghInstance; }

  window();
  bool init();
  bool is_valid() const { return _hwnd != 0; }

  BEGIN_FUNCTION_MAP
	  FUNCTION_3("InitGameState", init_game)
	  FUNCTION_2("LoadDataTable", load_data_table)
  END_FUNCTION_MAP

  sciter::value init_game(sciter::value fromFile,
						  sciter::value onComplete,
						  sciter::value extraParams);

  sciter::value load_data_table(sciter::value limit,
								 sciter::value onComplete);
};



