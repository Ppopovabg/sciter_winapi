#include "GameManager.h"
#include <sciter/aux-cvt.h> // a2w
#include <fstream>
#include <boost/format.hpp> // boost::format
#include <boost/format/format_fwd.hpp> // boost::wformat
#include "XmlParser.h"

#define GAMESAVE_FILENAME "gameData"

HANDLE _hTimer = NULL;
HANDLE _hTimerQueue = NULL;
TimerState _timerState = PAUSED;

void GameManager::insert_timeline_entry(const std::wstring& title, const date::year_month_day& date)
{
	date::month_day md{ date.month(), date.day() };
	std::wstringstream ws;
	ws << md;
	_timelineEntries.insert(_timelineEntries.begin(), std::vector<std::wstring>{ title, ws.str() });

	printf("%S %S\n", ws.str().c_str(), title.c_str());

	_wnd->call_function("UI.addTimelineEntry", title, ws.str());
}

/// Timer

void GameManager::event_start()
{
	_gameState = PLAYING;

	_eventStart = _gameTime;
	_eventEnd = (date::sys_days)_gameTime + _eventDuration;

	using namespace date;
	sys_days _days = (sys_days)_gameTime;

	for (int i = 1; i <= 5; ++i) {
		year_month_day ymd = _days + days(3*i);
		month_day date = month_day(ymd.month(), ymd.day());

		_activities.emplace(date, 0);

		if (date.month() == _gameTime.month()) {
			/*const sciter::dom::element root = sciter::dom::element::root_element(_wnd->get_hwnd());
			sciter::dom::element frame = root.find_first("frame");
			sciter::dom::element doc = frame.child(0);

			if (sciter::dom::element td = doc.find_first("table#calendar > tbody td#day-10")) {
				GUI_CODE_START
					SciterSetAttributeByName(td, "class", L"activity1");
				GUI_CODE_END
			}*/
			GUI_CODE_START
				_wnd->call_function("UI.markActivity", 10, 0);
			GUI_CODE_END
		}
	}

	display_dialog_arg("event-start",
		std::to_wstring(_eventDuration.count()),
		XmlParser::date_to_str(_eventEnd));

	insert_timeline_entry(L"Event has started", _gameTime);
}

void GameManager::gui_exec(std::function<void()> gui_block)
{
	sciter::sync::event evt;
	PostMessage(_wnd->get_hwnd(), WM_NULL, WPARAM(&evt), LPARAM(&gui_block));
	evt.wait(); // suspend worker thread until GUI will execute the block.
}

void GameManager::gametimer_start()
{
	_hTimerQueue = CreateTimerQueue();
	if (!_hTimerQueue) {
		printf("CreateTimerQueue failed (%d)\n", GetLastError());

		return;
	}

	if (!CreateTimerQueueTimer(&_hTimer, _hTimerQueue, (WAITORTIMERCALLBACK)timer_proc, NULL, 100, 3000, NULL))
		printf("CreateTimerQueueTimer failed (%d)\n", GetLastError());
}

void GameManager::gametimer_stop()
{
	if (_hTimerQueue) {
		if (!DeleteTimerQueue(_hTimerQueue))
			printf("DeleteTimerQueue failed (%d)\n", GetLastError());

		_hTimer = NULL;
		_hTimerQueue = NULL;
	}
}

void GameManager::gametimer_tick()
{
	if (_timerState == PAUSED)
		return;

	// refresh date on UI
	if (_wnd != 0) {
		HWND wnd = _wnd->get_hwnd();
		sciter::dom::element root = sciter::dom::element::root_element(wnd);

		if (sciter::dom::element cal = root.find_first("output#calendar")) {
			const std::wstring val = XmlParser::date_to_str(_gameTime);

			GUI_CODE_START
				cal.set_value(val);
			GUI_CODE_END
		}
		
		int monthDay = static_cast<unsigned>(_gameTime.day());
		if (monthDay == 1) {
			GUI_CODE_START
				_wnd->call_function("UI.changeCalendarMonth", getCurrentYear(), getCurrentMonth());
			GUI_CODE_END
		}

		/*sciter::dom::element frame = root.find_first("frame");
		sciter::dom::element doc = frame.child(0);
		std::string sel = "table#calendar td#day-" + std::to_string(monthDay);
		sciter::dom::element day = doc.find_first(sel.c_str());
		sciter::dom::element today = doc.find_first("td.today");*/

		GUI_CODE_START
			/*SciterSetAttributeByName(today, "class", L"");
			SciterSetAttributeByName(day, "class", L"today");*/
			_wnd->call_function("UI.changeCalendarDay", monthDay);
		GUI_CODE_END

	}

	if (_gameState == PLAYING) {
		// if event has ended
		if (_gameTime >= _eventEnd) {
			_gameState = INACTIVE;

			display_dialog_arg("event-end",
				XmlParser::date_to_str(_eventStart),
				XmlParser::date_to_str(_eventEnd));

			std::wstring title = L"Event has concluded";
			insert_timeline_entry(title, _gameTime);
		}
		else
		{
			// check if it's a certain day of the week
			date::year_month_weekday wd = static_cast<date::year_month_weekday>(_gameTime);
			/*if (wd.weekday() == date::weekday(7)) {
				int ppts = rand() % (20 - 10 + 1) + 10;

				display_dialog_arg("timed-event",
					std::to_wstring(ppts));

				std::wstring title = L"Result message";
				insert_timeline_entry(title, _gameTime);
			}*/
		}
	}

	// increment gametime
	_gameTime = (date::sys_days)_gameTime + _gameTick;

	//GameManager::save();
}

void CALLBACK GameManager::timer_proc(PVOID lpParam, BOOLEAN TimerOrWaitFired)
{
	GameManager& g = get();
	g.gametimer_tick();
}

/// Dialog

template<typename... Args>
void GameManager::display_dialog_arg(const char* id, const char* arg, const Args& ... args)
{
	if (!XmlParser::load_dialog(id, current_dialog))
		return;

	boost::format fmt(boost::format((const char*)aux::w2a(current_dialog.m_message)));

	fmt % arg;

	constexpr auto arg_c{ sizeof...(Args) };
	if (arg_c > 0) {
		const char* strings[arg_c]{ args... };

		for (unsigned i = 0; i < arg_c; ++i)
			fmt% strings[i];
	}

	std::wstring msg{ boost::str(fmt) };

	_wnd->call_function(
		"UI.showDialog",
		current_dialog.m_title,
		msg,
		current_dialog.m_options);
}

template<typename... Args>
void GameManager::display_dialog_arg(const char* id, const std::wstring& arg, const Args& ... args)
{
	if (!XmlParser::load_dialog(id, current_dialog))
		return;

	boost::wformat fmt(boost::wformat(current_dialog.m_message));

	fmt% arg;

	constexpr auto arg_c{ sizeof...(Args) };
	if (arg_c > 0) {
		std::vector<std::wstring> wstrings{ args... };

		for (unsigned i = 0; i < arg_c; ++i)
			fmt% wstrings[i];
	}

	std::wstring msg{ boost::str(fmt) };

	_wnd->call_function(
		"UI.showDialog",
		current_dialog.m_title,
		msg,
		current_dialog.m_options);
}

void GameManager::display_dialog(const char* id)
{
	if (!XmlParser::load_dialog(id, current_dialog))
		return;

	if (current_dialog.m_style_class != L"") {
		_wnd->call_function(
			"UI.showDialogClass",
			current_dialog.m_title,
			current_dialog.m_message,
			current_dialog.m_options,
			current_dialog.m_style_class);
	} else {
		_wnd->call_function(
			"UI.showDialog",
			current_dialog.m_title,
			current_dialog.m_message,
			current_dialog.m_options);
	}
}

void GameManager::set_dialog_callbacks()
{
	_dialogOpts[L"new-game"] = [&](const sciter::value& opt_data) {
		if (std::remove(GAMESAVE_FILENAME) == 0) {
			printf("Gamesave reset\n");
		}

		_gameState = SETUP;
		
		_records.clear();
		_timelineEntries.clear();

		_wnd->load_file(L"res:configure.htm");
	};
}

/// Script functions

sciter::value GameManager::displayDialogArg(const sciter::string id, sciter::value args)
{
	if (!XmlParser::load_dialog((const char*)aux::w2a(id), current_dialog))
		return sciter::value();

	if (args.length() < 1)
		return sciter::value();

	boost::wformat fmt(boost::wformat(current_dialog.m_message));

	args.isolate();
	for (unsigned i = 0; i < args.length(); ++i) {
		fmt % ((sciter::value)args[i]).get(L"");
	}

	std::wstring msg{ boost::str(fmt) };

	_wnd->call_function(
		"UI.showDialog",
		current_dialog.m_title,
		msg,
		current_dialog.m_options);

	return sciter::value();
}

sciter::value GameManager::displayDialog(const sciter::string id)
{
	display_dialog((char*)(const char*)aux::w2a(id));

	return sciter::value();
}

sciter::value GameManager::processDialogResult(const sciter::value opt_data)
{
	if (_dialogOpts.find(current_dialog.m_id) != _dialogOpts.end()) {
		_dialogOpts[current_dialog.m_id](opt_data);
	}

	return sciter::value();
}

bool GameManager::SaveFileExists() { return !std::ifstream(GAMESAVE_FILENAME).fail(); }

sciter::value GameManager::PauseTimer() { _timerState = PAUSED; return sciter::value(); }
sciter::value GameManager::UnpauseTimer() { _timerState = RUNNING; return sciter::value(); }

sciter::value GameManager::StartMenu()
{
	gametimer_stop();

	_wnd->load_file(L"res:start_menu.htm");

	return sciter::value();
}

int GameManager::getCurrentYear()
{
	return static_cast<int>(_gameTime.year());
}

int GameManager::getCurrentMonth()
{
	return static_cast<unsigned>(_gameTime.month());
}

// todo: move to worker
sciter::value GameManager::getDataFor(const int id) const
{
	sciter::value result;

	connection c;
	c.open();

	statement s;
	s.prepare(c, "select name, age, dob, country from data_table where rowid = ?;");

	s.bind(1, id);
	s.step();

	result[L"name"] = s.get_wstring(0);
	result[L"age"] = s.get_int(1);
	result[L"dob"] = s.get_wstring(2);
	result[L"country"] = s.get_wstring(3);

	return result;
}

///

/// Called at window initialization
void GameManager::init(window* wnd)
{
	_wnd = wnd;
	_gameState = SETUP;
	config = new Config();

	using namespace date;
	_gameTime = sys_days{ 2020_y / January / 1 };

	set_dialog_callbacks();
}

/// Called after game save has loaded or setup has finished
void GameManager::init_game()
{
	using namespace date;
	using namespace std::chrono_literals;

	_gameTick = days(1);
	_eventDuration = days(20);

	gametimer_start();

	if (_gameState == SETUP) {
		// new game
		config->debutDate = _gameTime;
	}
	else {
		// resume game

		_timerState = RUNNING;
	}
}

void GameManager::quit() { gametimer_stop(); }

/// File IO

void GameManager::save()
{
	std::ofstream ofs(GAMESAVE_FILENAME);

	if (ofs.fail()) {
		printf("Writing into '%s' failed\n", GAMESAVE_FILENAME);
		return;
	}

	boost::archive::text_oarchive oa(ofs);
	GameManager& g = get();
	oa << g;

	printf("Game state written into '%s'\n", GAMESAVE_FILENAME);
}

void GameManager::load()
{
	std::ifstream ifs(GAMESAVE_FILENAME);

	if (ifs.fail()) {
		printf("Reading from '%s' failed\n", GAMESAVE_FILENAME);
		return;
	}

	boost::archive::text_iarchive ia(ifs);
	GameManager& g = get();
	ia >> g;

	printf("Game state read from '%s'\n", GAMESAVE_FILENAME);
}

///

/// Get GameManager instance
GameManager& GameManager::get()
{
	static GameManager* g = new GameManager();
	return *g;
}